package edu.reflection;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@interface AnnotationForField {
    String value();
}


public class ReflectionTasks {

    @AnnotationForField("First")
    private String firstField = "AAA";

    @AnnotationForField("Second")
    public String secondField;

    private String thirdField;


    public void printOne(String stringValue) {
        System.out.println(stringValue + " One");
    }

    public String printTwo(String stringValue, int intValue) {
        return stringValue + " " + intValue;
    }

    public Double printThree(double doubleValue, long longValue) {
        return doubleValue + longValue;
    }

    public String[] myMethod(String str, int[] intArray) {

        String[] stringArray = new String[intArray.length];
        for (int i = 0; i < intArray.length; i++) {
            stringArray[i] = str + " added part --> " + intArray[i];
        }
        return stringArray;
    }

    public String[] myMethod(String[] stringArray) {
        for (int i = 0; i < stringArray.length; i++) {
            stringArray[i] += 1;
        }
        return stringArray;
    }
}
