package edu.reflection;


import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ObjectAnalyzer {

    private Object object;


    public ObjectAnalyzer(Object object) {
        this.object = object;
    }

    public String getObjectType() {
        return this.object.getClass().getSimpleName();
    }

    public String getPackage(){
        return this.object.getClass().getPackage().getName();
    }

    public String[] getMethodList() {
        final Method[] declaredMethods = this.object.getClass().getDeclaredMethods();
        String[] result = new String[declaredMethods.length];
        for (int i = 0; i < declaredMethods.length; i++) {
            result[i] = declaredMethods[i].getName() + " " + declaredMethods[i].getReturnType().getSimpleName();
        }
        return result;
    }

    public String[] getConstructorList() {
        final Constructor<?>[] declaredConstructors = this.object.getClass().getDeclaredConstructors();
        String[] result = new String[declaredConstructors.length];
        for (int i = 0; i < declaredConstructors.length; i++) {
            result[i] = declaredConstructors[i].getName()
                    .replace(object.getClass().getPackage().getName() + ".", "") + " with modifier "
                    + Modifier.toString(declaredConstructors[i].getModifiers())
                    + "\n";
        }
        return result;
    }

    public String[] getFieldList() {
        final Field[] declaredFields = this.object.getClass().getDeclaredFields();
        String[] result = new String[declaredFields.length];
        try {
            for (int i = 0; i < declaredFields.length; i++) {
                declaredFields[i].setAccessible(true);
                result[i] = declaredFields[i].getName()
                        + " with type " + declaredFields[i].getType().getSimpleName()
                        + " with modifier " + Modifier.toString(declaredFields[i].getModifiers())
                        + " and value " + declaredFields[i].get(object);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return result;
    }
}
