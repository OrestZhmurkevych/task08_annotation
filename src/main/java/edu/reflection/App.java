package edu.reflection;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class App {

    private static void taskOneAndTwo() {
        ReflectionTasks objectReflectionTasks = new ReflectionTasks();
        final Field[] declaredFields = objectReflectionTasks.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            try {
                if (field.isAnnotationPresent(AnnotationForField.class)) {
                    System.out.println("\nField with annotation: ");
                    System.out.println("Name of the field: " + field.getName());
                    System.out.println("Value of the field: " + field.get(objectReflectionTasks));
                    final AnnotationForField fieldAnnotation = field.getAnnotation(AnnotationForField.class);
                    System.out.println("Value of the annotation of the field: " + fieldAnnotation.value());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private static void taskThree() {
        try {
            ReflectionTasks object = new ReflectionTasks();
            final Method printOneMethod = object.getClass().getMethod("printOne", String.class);
            String parameterForFirstMethod = "First";
            printOneMethod.invoke(object, parameterForFirstMethod);

            final Method printTwoMethod = object.getClass().getMethod("printTwo", String.class, int.class);
            String parameterStringForSecondMethod = "Second";
            int parameterIntForSecondMethod = 7;
            final Object secondMethodResult = printTwoMethod.invoke(object, parameterStringForSecondMethod, parameterIntForSecondMethod);
            System.out.println("Second method result: " + secondMethodResult);

            final Method printThirdMethod = object.getClass().getMethod("printThree", double.class, long.class);
            double parameterDoubleForThirdMethod = 5.7;
            long parameterLongForThirdMethod = 17;
            final Double thirdMethodResult = (Double) printThirdMethod.invoke(object, parameterDoubleForThirdMethod, parameterLongForThirdMethod);
            System.out.println("Third method result: " + thirdMethodResult);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    public static Map<Class, Object> getClassValueMap(){
        Map<Class, Object> classValueMap = new HashMap<>();
        classValueMap.put(Integer.class, 22);
        classValueMap.put(String.class, "Apple");
        classValueMap.put(Double.class, 17.1);
        classValueMap.put(Character.class, '@');
        classValueMap.put(long.class, 173L);
        classValueMap.put(int.class, 151);
        return classValueMap;
    }

    private static void taskFour() throws Exception {
        try {
            ReflectionTasks object = new ReflectionTasks();
            Field field = object.getClass().getDeclaredField("thirdField");
            field.setAccessible(true);
            final Class<?> fieldClass = field.getType();
            final Map<Class, Object> classValueMap = getClassValueMap();
            final Object valueToSet = classValueMap.get(fieldClass);
            if(valueToSet == null){
                throw new Exception("Unsupported type of possible value");
            }
            field.set(object, valueToSet);
            System.out.println(field.get(object));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static void printArray(String[] stringArray){
        for (String str:stringArray) {
            System.out.print(str + " ");
            System.out.println();
        }
    }

    public static void printIntArray(int[] intArray){
        for (int item:intArray) {
            System.out.print(item + " ");
        }
        System.out.println();
    }

    private static void taskFive() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        ReflectionTasks object = new ReflectionTasks();
        final Class<? extends ReflectionTasks> classObject = object.getClass();
        final Method myMethodWithOneParameter = classObject.getDeclaredMethod("myMethod", String[].class);
        final Method myMethodWithTwoParameters = classObject.getDeclaredMethod("myMethod", String.class, int[].class);

        final int[] intArray = {1, 2, 3, 4, 5};
        Object[] parametersForMethodWithTwoParams = new Object[]{"SomeString", intArray};
        final String[] resultOfMyMethodWithTwoParameters = (String[]) myMethodWithTwoParameters.invoke(object, parametersForMethodWithTwoParams);
        System.out.println("Result of myMethod with two parameters: ");
        printArray(resultOfMyMethodWithTwoParameters);

        String[] stringArray = new String[]{"AAA", "BBB", "CCC"};
        Object[] parametersForMethodWithOneParam = new Object[]{stringArray};
        final String[] resultOfmyMethodWithOneParameter = (String[])myMethodWithOneParameter.invoke(object, parametersForMethodWithOneParam);
        System.out.println("Result of myMethod with one parameter: ");
        printArray(resultOfmyMethodWithOneParameter);
    }

    private static void taskSix() {
        ReflectionTasks object = new ReflectionTasks();
        ObjectAnalyzer analyzer = new ObjectAnalyzer(object);


        System.out.println("\nClass type/name: " + analyzer.getObjectType());

        System.out.println("Package: " + analyzer.getPackage() + "\n");

        final String[] fieldList = analyzer.getFieldList();
        System.out.println("Fields, their types, modifiers and values: ");
        printArray(fieldList);

        final String[] constructorsList = analyzer.getConstructorList();
        System.out.println("\nConstructors of the class: ");
        printArray(constructorsList);

        final String[] methodList = analyzer.getMethodList();
        System.out.println("Methods and their return type: ");
        printArray(methodList);
    }

    public static void main(String[] args) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Task 1 and 2(2 and 3 in the task list) - enter number 1");
        System.out.println("Task 3(4 in the task list) - enter number 2");
        System.out.println("Task 4(5 in the task list) - enter number 3");
        System.out.println("Task 5(6 in the task list) - enter number 4");
        System.out.println("Task 6(7 in the task list) - enter number 5");
        System.out.print("Enter the number of task which you want to execute --> ");
        final int numberOfTask = Integer.parseInt(br.readLine());
        switch(numberOfTask) {
            case 1: taskOneAndTwo();
                break;
            case 2: taskThree();
                break;
            case 3: taskFour();
                break;
            case 4: taskFive();
                break;
            case 5: taskSix();
                break;
            default: throw new IllegalArgumentException("The entered number is not supported");
        }
    }
}

